# H600-Wireless-Headset-Setting

The H600 older generation is today called H800. 


## Media
![](h600-older-gen.jpg)
![](linux.png)

## Pairing


![](pairing/1644478636-screenshot.png)

![](pairing/1644478690-screenshot.png)



## Settings with Pavucontrol (USB dongle)

![](screenshot-1620068944.png)
![](screenshot-1620068948.png)
![](screenshot-1620068954.png)
![](screenshot-1620068966.png)
![](screenshot-1620068969.png)

![](1626639549-screenshot.png)


